<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9610b465-f8d4-468b-aad5-e664210245d0(Task_2_Language.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="c466a2ad-dc4f-45af-92b2-f3b6af9366a6" name="Task_2_Language" version="-1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="5" />
  </languages>
  <imports />
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="c466a2ad-dc4f-45af-92b2-f3b6af9366a6" name="Task_2_Language">
      <concept id="7256338513672090652" name="Task_2_Language.structure.Iterate" flags="ng" index="3hQClM" />
      <concept id="7256338513672092430" name="Task_2_Language.structure.Script" flags="ng" index="3hQCDw">
        <child id="7256338513672092431" name="body" index="3hQCDx" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="6iNGmUPqjFp">
    <property role="TrG5h" value="Test" />
    <node concept="3Tm1VV" id="6iNGmUPqjFq" role="1B3o_S" />
  </node>
  <node concept="3hQCDw" id="6iNGmUPqjRA">
    <property role="TrG5h" value="MyScript" />
    <node concept="3hQClM" id="6iNGmUPqjRB" role="3hQCDx" />
    <node concept="3hQClM" id="6iNGmUPqjRF" role="3hQCDx" />
    <node concept="3hQClM" id="6iNGmUPqjRM" role="3hQCDx" />
    <node concept="3hQClM" id="6iNGmUPqjRW" role="3hQCDx" />
    <node concept="3hQClM" id="6iNGmUPqjS9" role="3hQCDx" />
    <node concept="3hQClM" id="6iNGmUPqjSp" role="3hQCDx" />
  </node>
</model>

