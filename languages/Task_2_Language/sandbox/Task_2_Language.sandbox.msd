<?xml version="1.0" encoding="UTF-8"?>
<solution name="Task_2_Language.sandbox" uuid="f28015ac-4c6e-422c-870b-0334443b43db" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:c466a2ad-dc4f-45af-92b2-f3b6af9366a6:Task_2_Language" version="-1" />
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="5" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="f28015ac-4c6e-422c-870b-0334443b43db(Task_2_Language.sandbox)" version="0" />
  </dependencyVersions>
</solution>

